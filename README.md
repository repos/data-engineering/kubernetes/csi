# Kubernetes Container Storage Interface (CSI)

This repository contains the WMF build pipeline for the Kubernetes Container Storage Interface (CSI) containers.

It builds and publishes containers for the following CSI components:

* csi-attacher       - https://github.com/kubernetes-csi/external-attacher
* csi-provisioner    - https://github.com/kubernetes-csi/external-provisioner
* csi-snapshotter    - https://github.com/kubernetes-csi/external-snapshotter
* csi-resizer        - https://github.com/kubernetes-csi/external-resizer
* csi-node-registrar - https://github.com/kubernetes-csi/node-driver-registrar
